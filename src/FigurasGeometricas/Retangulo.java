package FigurasGeometricas;

public class Retangulo extends FiguraGeometrica {
	private double base;
	private double altura;
	private Vertice centro;
	
	public Retangulo() {
		super(4);
	}

	public Retangulo(double x, double y, double b, double h) {
		super(4);
		this.base = b;
		this.altura = h;
		gerarVertices(x, y);
		Vertice v[]=getVertices();
		gerarArestas(v);
		calculaCentro();
	}

	public void gerarVertices(double x, double y) {
		Vertice v[] = new Vertice[getNLados()];
		v[0] = new Vertice(x, y);
		v[1] = new Vertice(x + base, y);
		v[2] = new Vertice(x + base, y + altura);
		v[3] = new Vertice(x, y + altura);
		setVertices(v);
	}

	public void gerarArestas(Vertice v[]) {
		Aresta a[] = new Aresta[getNLados()];
		a[0] = new Aresta(v[0], v[1]);
		a[1] = new Aresta(v[1], v[2]);
		a[2] = new Aresta(v[2], v[3]);
		a[3] = new Aresta(v[3], v[0]);
		setArestas(a);
	}

	public double calculaArea() {return base * altura;}

	public double calculaPerimetro() {return (base * 2 + altura * 2);}
	
	public void calculaCentro(){
		double x = vertices[0].getX() +base/2;
		double y = vertices[0].getY()+altura/2;
		centro = new Vertice(x, y);
	}
	public void translacao(double x, double y){
		centro.setX(centro.getX()+x);
		centro.setY(centro.getY()+x);
		
		for(int i=0; i<vertices.length; i++){
			vertices[i].setX(vertices[i].getX() + x);
			vertices[i].setY(vertices[i].getY() + y);
			
		}
	}
	public void rotacao(double graus){
		double grau = graus*(Math.PI/180);
		for (int i = 0; i < vertices.length; i++) {
			double x = vertices[i].getX();
			double y = vertices[i].getY();
			double xTemp = x * Math.cos(grau) - y * Math.sin(grau);
			double yTemp = x * Math.sin(grau) + y * Math.cos(grau);
			vertices[i].setX(xTemp);
			vertices[i].setY(yTemp);
		}
		
	}
	public void alteraTamanho(double percentual){
		percentual/=100;
		base = base*percentual;
		altura = altura*percentual;
		gerarVertices(base, altura);
	}
}
	
