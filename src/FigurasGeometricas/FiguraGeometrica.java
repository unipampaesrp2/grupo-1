package FigurasGeometricas;

public abstract class FiguraGeometrica {
    private int nLados;
    protected Vertice vertices[];
    private Aresta arestas[];
    private String corBorda;
    private String corPreenchimento;
    
    public FiguraGeometrica(int nLados){
		this.nLados = nLados;
		this.arestas = new Aresta[nLados];
		this.vertices = new Vertice[nLados];
	}
	public int getNLados() {return nLados;}
	
	public void setNLados(int nLados) {this.nLados = nLados;}
	
	public Vertice[] getVertices() {return vertices;}
	
	public void setVertices(Vertice[] vertices) {this.vertices = vertices;}
	
	public Aresta[] getArestas() {return arestas;}
	
	public void setArestas(Aresta[] arestas) {this.arestas = arestas;}
	
	public String getCorBorda() {return corBorda;}
	
	public void setCorBorda(String corBorda) {this.corBorda = corBorda;}
	
	public String getCorPreenchimento() {return corPreenchimento;}
	
	public void setCorPreenchimento(String corPreenchimento) {this.corPreenchimento = corPreenchimento;}
	
    public abstract double calculaArea();
   
    public abstract double calculaPerimetro();
   
    public String getExibeVertices(){
	   String v = "";
	   for(int i=0; i<nLados; i++ ){
		   v+= "Vertice: " +(i+1)  +"  x: "+ vertices[i].getX() + "  Y: "+ vertices[i].getY()+ "\n";
	   }
	   return v;
   }
    public String getExibeAresta(){
	   String a = "";
	   for(int i=0; i<nLados; i++ ){
		   a+= "Aresta " +(i+1) + ": " + arestas[i].getV1().getX() + " , " + arestas[i].getV1().getY() + " -- ";
		   a+= arestas[i].getV2().getX() + " , " + arestas[i].getV2().getY() + "\n";
	   }
	   return a;
   }
    
  
}

