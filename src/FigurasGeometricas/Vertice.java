package FigurasGeometricas;

public class Vertice {
	private double x;
    private double y;
   
    public Vertice(double x, double y) {
        this.x = x;
        this.y = y;
        }

    public double getX() {
        return x;

    }

    public void setX(double novoX) {
        this.x = novoX;
        
    }

    public double getY() {
        return y;
    }
    public void setY(double novoY) {
        this.y = novoY;
    }
    
	
}
