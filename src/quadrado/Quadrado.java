/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quadrado;

/**
 *
 * @author Lincoln
 */

import java.awt.Color;
import java.util.ArrayList;

public class Quadrado extends FormaGeometrica{
/**
* 
*/

	private ArrayList<Vertice> listaPonto;
	private ArrayList<Aresta> listaArestas;	

	
	
/**
* 
*/
	public Quadrado() {
		
		this.listaPonto = new ArrayList<Vertice>();
	}

	
	
/**
* 
* @param vertice
*/
	public void adicionarPontosNaLista(Vertice vertice) {
		this.listaPonto.add(vertice);
	}

	
/**
* 	
* @return
*/
	
	public ArrayList<Vertice> getListaDePontos() {
		return this.listaPonto;
	}

	
/**
* 	
* @param distancia
* @return
*/
	

	public double calcularArea(double distancia) {
		
		double lado = 10;
		
		double termo1 = lado * lado;
		
		return (termo1);
	}

	
	
	public double calculaAresta() {
		
		double distancia = 0.0;
		
		if (this.listaArestas == null) {
			
			definirArestas();
		
		} else {

			double termo1 = (listaArestas.get(0).getPonto1().getX() - listaArestas.get(0).getPonto2().getX())
					* (listaArestas.get(0).getPonto1().getX() - listaArestas
							.get(0).getPonto2().getX());

			double termo2 = (listaArestas.get(0).getPonto1().getY() - listaArestas
					.get(0).getPonto2().getY())
					* (listaArestas.get(0).getPonto1().getY() - listaArestas
							.get(0).getPonto2().getY());

			double termo3 = termo1 + termo2;

			distancia = Math.sqrt(termo3);

			return distancia;
		}
		return distancia;
	}

	
	
	
/**
* Metodo que calcula o Perimetro de um quadrado
* @param lado
* @return
*/
	public double calcularPerimetro(double distancia) {
		double lado = 10;
                double termo1 = (4 * lado);
                
                return (termo1);
                        
            
	}

/**
 * 
 */
	private void definirArestas() {
		
		this.listaArestas = new ArrayList<>();

		for (int count = 0; count < this.listaPonto.size(); count++) {
			if (count + 1 < (this.listaPonto.size() - 1)) {
				this.listaArestas.add(new Aresta(this.listaPonto.get(count),
						this.listaPonto.get(count + 1)));
			} else {
				this.listaArestas.add(new Aresta(this.listaPonto.get(count),
						this.listaPonto.get(0)));
			}
		}
	}

/**
* 
* @return
*/
	public ArrayList<Aresta> recuperaArestas() {
		this.definirArestas();
		return this.listaArestas;
	}
/**
* 
* @return
*/
	public double mostraVertices() {
		return 0;
	}

    

}
