/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quadrado;

/**
 *
 * @author Lincoln
 */




public class Vertice extends Quadrado {
	/**
	 * 
	 */
		
	private double x;
	private double y;
	
	
	public Vertice(double x, double y) {
		this.x = x;
		this.y = y;
	}

    
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}


	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
}



