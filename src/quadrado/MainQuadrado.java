/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quadrado;

/**
 *
 * @author Lincoln
 */
import java.awt.Color;

import java.util.ArrayList;
import java.util.Scanner;

public class MainQuadrado {
	private static Scanner scanner;
	private static int NUMERO_LIMITE = 4 ;
	

	public static void main(String[] args) {
		scanner = new Scanner(System.in);

		Quadrado quadrado =  new Quadrado();

		System.out.println("Digitar o c�digo da cor da borda: ");
		System.out.println("[1] VERMELHO - [2] AZUL - [3] AMARELO - [4] PRETO");
		int codigoCor = scanner.nextInt();
		switch (codigoCor) {
		case 1:
			quadrado.setCorBorda(Color.RED);
			break;
		case 2:
			quadrado.setCorBorda(Color.BLUE);
		case 3:
			quadrado.setCorBorda(Color.YELLOW);
			break;
		default:
			quadrado.setCorBorda(Color.BLACK);
		}
		
		System.out
				.println("Digitar o c�digo da cor do preenchimento: ");
		System.out.println("[1] VERMELHO - [2] AZUL - [3] AMARELO - [4] BRANCO");
		int codigoCor2 = scanner.nextInt();
		switch (codigoCor2) {
		case 1:
			quadrado.setCorDoPreenchimento(Color.RED);
			break;
		case 2:
			quadrado.setCorDoPreenchimento(Color.BLUE);
		case 3:
			quadrado.setCorDoPreenchimento(Color.YELLOW);
			break;
		default:
			quadrado.setCorDoPreenchimento(Color.WHITE);
		}
			System.out.printf("Digite a coordenada x: \n");
			double x = scanner.nextDouble();
			System.out.printf("Digite a coordenada y: \n");
			double y = scanner.nextDouble();

			quadrado.adicionarPontosNaLista(new Vertice(x, y));
                        quadrado.adicionarPontosNaLista(new Vertice(x+10, y));
                        quadrado.adicionarPontosNaLista(new Vertice(x, y+10));
                        quadrado.adicionarPontosNaLista(new Vertice(x+10, y+10));
		

		ArrayList<Aresta> arestas = quadrado.recuperaArestas();
		double distancia = quadrado.calculaAresta();

		int countApoio = 1;
		for (Aresta arestaTemp : arestas) {
			System.out.printf("Aresta %d: V1(%.2f , %.2f) - V2(%.2f , %.2f)\n",
					countApoio, arestaTemp.getPonto1().getX(), arestaTemp
							.getPonto1().getY(), arestaTemp.getPonto2().getX(),
					arestaTemp.getPonto2().getY());
			countApoio++;
		}
		double areaTemp = 0.0;

		System.out.printf("Aresta do quadrado\n", distancia);
		areaTemp = quadrado.calcularArea(distancia);
		
		System.out.printf("�rea do quadrado: %.2f\n", areaTemp);
		
		
		System.out.println("Per�metro do quadrado "+quadrado.calcularPerimetro(10));
				

	}
}
