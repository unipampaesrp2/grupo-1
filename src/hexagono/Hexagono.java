package hexagono;

import java.util.ArrayList;
/**
* @author eduardo
* @version 1
* @since 2014
*/
public class Hexagono extends FormaGeometrica{
/**
* 
*/

	private ArrayList<Vertice> listaPonto;
	private ArrayList<Aresta> listaArestas;	

	
	
/**
* 
*/
	public Hexagono() {
		
		this.listaPonto = new ArrayList<Vertice>();
	}

	
	
/**
* 
* @param vertice
*/
	public void adicionarPontosNaLista(Vertice vertice) {
		this.listaPonto.add(vertice);
	}

	
/**
* 	
* @return
*/
	
	public ArrayList<Vertice> getListaDePontos() {
		return this.listaPonto;
	}

	
/**
* 	
* @param distancia
* @return
*/
	

	public double calcularArea(double distancia) {
		
		double raizDe3 = Math.sqrt(3);
		
		double termo1 = raizDe3 * 3;
		
		return ((termo1 * (distancia * distancia) / 2));
	}

	
	
	
/**
* Metodo que calcula o tamanho de uma aresta de um hexagono
* @return
*/
	
	public double calculaAresta() {
		
		double distancia = 0.0;
		
		if (this.listaArestas == null) {
			
			definirArestas();
		
		} else {

			double termo1 = (listaArestas.get(0).getPonto1().getX() - listaArestas.get(0).getPonto2().getX())
					* (listaArestas.get(0).getPonto1().getX() - listaArestas
							.get(0).getPonto2().getX());

			double termo2 = (listaArestas.get(0).getPonto1().getY() - listaArestas
					.get(0).getPonto2().getY())
					* (listaArestas.get(0).getPonto1().getY() - listaArestas
							.get(0).getPonto2().getY());

			double termo3 = termo1 + termo2;

			distancia = Math.sqrt(termo3);

			return distancia;
		}
		return distancia;
	}

	
	
	
/**
* Metodo que calcula o Perimetro de um Hexagono
* @param distancia
* @return
*/
	public double calcularPerimetro(double distancia) {
		return (6 * distancia);

	}

/**
 * 
 */
	private void definirArestas() {
		
		this.listaArestas = new ArrayList<>();

		for (int count = 0; count < this.listaPonto.size(); count++) {
			if (count + 1 < (this.listaPonto.size() - 1)) {
				this.listaArestas.add(new Aresta(this.listaPonto.get(count),
						this.listaPonto.get(count + 1)));
			} else {
				this.listaArestas.add(new Aresta(this.listaPonto.get(count),
						this.listaPonto.get(0)));
			}
		}
	}

/**
* 
* @return
*/
	public ArrayList<Aresta> recuperaArestas() {
		this.definirArestas();
		return this.listaArestas;
	}
/**
* 
* @return
*/
	public double mostraVertices() {
		return 0;
	}

}
