package hexagono;

import hexagono.Hexagono;
import hexagono.Vertice;

/**
 * @author eduardo
 * @version 1
 * @since 2014
 */
public class Aresta extends Hexagono{
/**
 * 
*/
	private Vertice ponto1;
	private Vertice ponto2;
/**
* 
* @param ponto1
* @param ponto2
*/
	public Aresta(Vertice ponto1, Vertice ponto2) {
		this.ponto1 = ponto1;
		this.ponto2 = ponto2;
	}
/**
* 
* @return
*/
	public Vertice getPonto1() {
		return this.ponto1;
	}
/**
* 
* @return
*/
	public Vertice getPonto2() {
		return this.ponto2;
	}
}
