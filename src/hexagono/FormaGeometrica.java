package hexagono;

import java.awt.Color;

/**
 * @author eduardo
 * @version 1
 * @since 2014
 */
public abstract class FormaGeometrica {
	private Color corDaBorda;
	private Color corDoPreenchimento;
	private int numeroLados;
	
/**
 * 
 * @return
 */
	public Color getCorDaBorda() {
		return corDaBorda;
	}

/**
 * 
 * @param corBorda
 */
	public void setCorBorda(Color corBorda) {
		this.corDaBorda = corBorda;

	}

/**
 * 
 * @return
 */
	public Color getCorDoPreenchimento() {
		return corDoPreenchimento;
	}

/**
 * 
 * @param corDoPreenchimento
 */
	public void setCorDoPreenchimento(Color corDoPreenchimento) {
		this.corDoPreenchimento = corDoPreenchimento;
	}

/**
 * 
 * @return
 */
	public int getNumeroLados() {
		return numeroLados;
	}

/**
 * 
 * @param numeroLados
 */
	public void setNumeroLados(int numeroLados) {
		this.numeroLados = numeroLados;
	}
	
		
		
		
	}
	


