package hexagono;

import hexagono.Hexagono;

/**
 * @author eduardo
 * @version 1
 * @since 2014
 */

public class Vertice extends Hexagono {
	/**
	 * 
	 */
		
	private double x;
	private double y;
	
	/**
	 * 	
	 * @param x
	 * @param y
	 */
		

	public Vertice(double x, double y) {
		this.x = x;
		this.y = y;
	}
	/**
	 * 
	 * @return
	 */
	public double getX() {
		return x;
	}
	/**
	 * 
	 * @param x
	 */

	public void setX(double x) {
		this.x = x;
	}
	/**
	 * 
	 * @return
	 */

	public double getY() {
		return y;
	}
	/**
	 * 
	 * @param y
	 */

	public void setY(double y) {
		this.y = y;
	}
}



