package hexagono;
 
/**
 * 
 */
import java.awt.Color;

import java.util.ArrayList;
import java.util.Scanner;
 
 /**
 * @author eduardo
 * @version 1
 * @since 2014
 */

public class MainHexagono {
	
	private static Scanner scanner;
	private static int NUMERO_LIMITE = 6;
	/**
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		scanner = new Scanner(System.in);

		Hexagono hexagono = new Hexagono();

		System.out.println("Digite o código da cor de borda desejável: ");
		System.out.println("[1] VERMELHO - [2] AZUL - [3] AMARELO");
		int codigoCor = scanner.nextInt();
		switch (codigoCor) {
		case 1:
			hexagono.setCorBorda(Color.RED);
			break;
		case 2:
			hexagono.setCorBorda(Color.BLUE);
		case 3:
			hexagono.setCorBorda(Color.YELLOW);
			break;
		default:
			hexagono.setCorBorda(Color.BLACK);
		}
		
		System.out
				.println("Digite o código da cor de preenchimento desejável: ");
		System.out.println("[1] VERMELHO - [2] AZUL - [3] AMARELO");
		int codigoCor2 = scanner.nextInt();
		switch (codigoCor2) {
		case 1:
			hexagono.setCorDoPreenchimento(Color.RED);
			break;
		case 2:
			hexagono.setCorDoPreenchimento(Color.BLUE);
		case 3:
			hexagono.setCorDoPreenchimento(Color.YELLOW);
			break;
		default:
			hexagono.setCorDoPreenchimento(Color.WHITE);
		}

		for (int count = 0; count < NUMERO_LIMITE; count++) {
			
			System.out.printf("Digite a coordenada x: \n");
			double x = scanner.nextDouble();
			System.out.printf("Digite a coordenada y: \n");
			double y = scanner.nextDouble();

			hexagono.adicionarPontosNaLista(new Vertice(x, y));
		}

		ArrayList<Aresta> arestas = hexagono.recuperaArestas();
		double distancia = hexagono.calculaAresta();

		int countApoio = 1;
		for (Aresta arestaTemp : arestas) {
			System.out.printf("Aresta %d: V1(%.2f , %.2f) - V2(%.2f , %.2f)\n",
					countApoio, arestaTemp.getPonto1().getX(), arestaTemp
							.getPonto1().getY(), arestaTemp.getPonto2().getX(),
					arestaTemp.getPonto2().getY());
			countApoio++;
		}
		double areaTemp = 0.0;

		System.out.printf("Aresta do Hexágono: %.2f\n", distancia);
		areaTemp = hexagono.calcularArea(distancia);
		
		System.out.printf("Área do Hexágono: %.2f\n", areaTemp);
		
		
		System.out.printf("Perimetro do Hexágono: %.2f\n"
				, hexagono.calcularPerimetro(distancia));

	}
}
