package Triangulo;

import FigurasGeometricas.Aresta;
import FigurasGeometricas.FiguraGeometrica;
import FigurasGeometricas.Vertice;

public class Triangulo extends FiguraGeometrica{
	private double base;
	private double altura;
	private Vertice centro;
	
	public Triangulo(){
		super(3);
	}
	public Triangulo(double x, double y, double b, double h) {
		super(3);
		this.base = b;
		this.altura = h;
		gerarVertices(x, y);
		Vertice v[]=getVertices();
		gerarArestas(v);
		calculaCentro();
	}
	public void gerarVertices(double x, double y) {
		Vertice v[] = new Vertice[getNLados()];
		v[0] = new Vertice(x, y);
		v[1] = new Vertice(x + base, y);
		v[2] = new Vertice(x + base, y + altura);
		setVertices(v);
	}

	public void gerarArestas(Vertice v[]) {
		Aresta a[] = new Aresta[getNLados()];
		a[0] = new Aresta(v[0], v[1]);
		a[1] = new Aresta(v[1], v[2]);
		a[2] = new Aresta(v[2], v[0]);
		setArestas(a);
	}


	@Override
	public double calculaArea() {
		return base *altura/2;
	
	}

	@Override
	public double calculaPerimetro() {
		return altura*2 +base;
	
	}
	public void calculaCentro(){
		double x = base/2;
		double y = altura/2;
		centro = new Vertice(x, y);
	}
	public void Translacao(double x, double y){
		for(int i=0; i<vertices.length; i++){
			double xTemp;
			double yTemp;
			xTemp = vertices[i].getX();
			yTemp = vertices[i].getY();
			xTemp = x;
			yTemp = y;
			vertices[i].setX(xTemp);
			vertices[i].setY(yTemp);
			
		}
	}
	public void Rotacao(double graus){
		double x=0, y = 0;
		double grau = graus*(Math.PI/180);
		Translacao(centro.getX()*-1, centro.getY()*-1);
		for(int i=0; i<vertices.length; i++){
			double xTemp = vertices[i].getX();
			double yTemp = vertices[i].getY();
			xTemp = x * Math.cos(graus) - y * Math.sin(graus);
			yTemp = x * Math.sin(graus) + y * Math.cos(graus);
			vertices[i].setX(xTemp);
			vertices[i].setY(yTemp);
		}
		Translacao(centro.getX(), centro.getY());
		gerarArestas(vertices);
	}
	public void alteraTamanho(double percentual){
		percentual/=100;
		double tempB = base*percentual;
		double tempH = altura*percentual;
		gerarVertices(tempB, tempH);
		gerarArestas(vertices);
	}


}
